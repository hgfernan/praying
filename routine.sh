#! /bin/bash

. ./setvars.sh

for file in ${DATA_DIR}/*.out; do
	echo $file; /bin/cp -p -v ${file} ${file}".prev" 
done

./stats.sh | tee ${DATA_DIR}/stats.out 
sleep 5 

for ind in {4..25} ; do
	echo $ind
	INDF=`printf "%02d" $ind`
	./show.sh $ind | tee ${DATA_DIR}/show_${INDF}.out
done

for file in ${DATA_DIR}/show_*.out; do
	echo $file

	prev=${file}".prev"

	if ( test ! -f ${prev} ) ; then
		touch ${prev}
	fi

	diff -c ${prev} ${file} | grep "+ "  
	sleep 2
done

file=${DATA_DIR}/"stats.out"
echo ${file} 
diff -c ${file}".prev" ${file} | grep "! "

git add .

cd - 

