#! /bin/bash

set +x 

. ./setvars.sh 

cd ${DATA_DIR}

for ind in {0..22} ; do
	SRCH="0;"${ind}"$"
	CMD=`printf "cat ""${INPUT_F}"" | grep \"${SRCH}\"\n" `
	
	echo -n ${ind}" "
	eval ${CMD} | wc -l
done

cd - > /dev/null


