#! /usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Mar 15 10:43:24 2019

@author: hilton
From 
https://docs.python.org/3.5/library/csv.html
"""

import os     # replace 
import sys    # argv, exit()
import csv    # class reader
import time   # time()
import random # seed(), randrange()

def main(argv):
    fname = 'recuerdos.csv'
    if len(sys.argv) > 1:
        fname = argv[1]
        
    # 
    # 
    # Input
    # 
    
    with open(fname, 'r') as csvfile:
        header = 'Numero;Nombre;Local;Family;Frequency'
    #    header = 'Numero;Nombre;Local;Family;Banana'
        field_names = header.split(';')
    #    print(field_names)
        data = []
#        spamreader = csv.DictReader(csvfile, fieldnames = field_names, 
#                                    delimiter=';', 
#                                    quoting =  csv.QUOTE_NONNUMERIC)
        spamreader = csv.DictReader(csvfile, 
#                                    delimiter=';')
                                    delimiter=';', 
                                    quoting =  csv.QUOTE_NONNUMERIC)
#        spamreader = csv.DictReader(csvfile, delimiter=';')
        
        rowNo = 0
        for row in spamreader:
            print(f'row {row}')
            
            rowNo += 1
#            if rowNo == 1:
#                continue
            
            line = {}
            for key in row.keys():
                print(f'row[{key}] {row[key]}')
                if key in ['Nombre', 'Local']:
                    line[key] = row[key].strip()
                    continue
                
#                print(f'row[{key}] {row[key]}')
                line[key] = int(row[key])
                
            data.append(line)

    # TODO data selection
#    random.seed((1 << 23) - 1)
    my_seed = round(time.time())
    print("seed: {0}".format(my_seed))
    random.seed(my_seed)
    
    sel = []
    perm_count = 0
    tot_sel = 30
#    for ind in range(len(data)):
#        if data[ind]['Family'] == 1:
#            perm_count += 1
#            sel.append(ind)

    count_sel = perm_count
    while count_sel < tot_sel:
        ind = random.randrange(0, len(data))
        
        if not (ind in sel):
            count_sel += 1
            sel.append(ind)
    
    # TODO report output
    sel = sorted(sel)
    for ind in sel:
        print (data[ind]['Numero'], data[ind]['Nombre'], data[ind]['Local'], )
        data[ind]['Frequency'] += 1

    # TODO data output
    fname_bak = fname + '.bak'
    os.replace(fname, fname_bak)

    for ind in range(len(field_names)):
        if (type(field_names[ind]) == "<class 'str'>"):
            field_names[ind] = bytes(field_names[ind], 'utf8')
    
    with open(fname, 'w') as csvfile:
        writer = csv.DictWriter(csvfile, fieldnames = field_names, \
                                delimiter = ';', 
                                quoting =  csv.QUOTE_NONNUMERIC)
    
        writer.writeheader()
        csvfile.flush()
        for row in data:
            writer.writerow(row)

    # Normal function termination
    return 0
     
if __name__ == '__main__':
    sys.exit(main(sys.argv))
    
